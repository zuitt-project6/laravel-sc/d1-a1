<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" type="text/css" href="{{asset('/css/app.css')}}">
   <script src="{{ asset('js/app.js') }}" defer></script>
   <title>Laravel App</title>
</head>
<body>
   @include('inc.navbar')
   <div class='container'>
   @yield('content')
   </div>
</body>
</html>

<!-- php artisan ui vue --auth  -->