@extends('layouts.app')

@section('content')
   
   <h1>{{$post->title}}</h1>
   <p class="text-muted">Written on {{$post->created_at}}</p>

   <div class="alert alert-primary">
      <p>{{$post->body}}</p>
      <div class="d-flex justify-content-around">
         @if(!Auth::guest())
         <button type="button" class="btn btn-outline-primary border-0" data-toggle="modal" data-target="#commentModal">
            Comment
         </button>
         @endif
      <a class="btn-outline-dark btn-sm px-4" href="{{ url()->previous() }}">go back to previous page</a>
      </div>
   </div>
   {{-- COMMENTS --}}

   @if(count($post->comments)>0)
      <h5>Comments:</h5>
      
      @foreach($post->comments as $comment)
      <div class="alert alert-secondary">
         <p class="text-muted">{{$comment->user->name}}: {{$comment->created_at}}</p>
         <h6>{{$comment->content}}</h6>       
      </div>
      @endforeach
   @endif

{{-- MODAL  --}}
   <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form class="modal-content" action="/posts/{{$post->id}}/comment" method="POST">
         @csrf
          <div class="modal-header">
            <h5 class="modal-title" id="commentModal">Comment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <textarea name="content" rows="10" placeholder="Write comment here" class="w-100"></textarea>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-outline-primary border-0">Post comment</button>
          </div>
        </form>
      </div>
    </div>
@endsection