<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Hello Kity</title>
   <style>
      *{
         color:azure;
      }
   </style>
</head>
<body style="height:100vh; background:url(https://wallpaperaccess.com/full/400275.jpg);background-size:cover">

   <h1 style="color:azure">{{$front_end}}</h1>
   @if(count($topics) > 0)
      @foreach($topics as $topic)
         <li>{{$topic}}</li>
      @endforeach
   @endif
</body>
</html>