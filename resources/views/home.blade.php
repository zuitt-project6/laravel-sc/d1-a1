@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header text-center">My Posts</div>

                @if(count($posts) > 0)

                    <table class="table table-striped">
                        <thead class="sticky-top bg-dark text-light">
                            <tr>
                                <th>Title</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->title}}</td>
                                    <td><a class="btn-outline-primary btn border-0" href="/posts/{{$post->id}}/edit"><u><i class="bi bi-pencil-square"></i></u></a>
                                    </td>
                                    <td>
                                        <form action="{{action('PostController@destroy', [$post->id])}}" method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-outline-danger border-0" type="submit"><u><i class="bi bi-trash3-fill"></i></u></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <h6 class="text-center text-muted">No posts found!</h6>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection
