@extends('layouts.app')

@section('content')
@if(!Auth::guest())
   @if(Auth::user()->id === $post->user_id)
   <h1>Edit Post</h1>

   <form action="{{action('PostController@update', [$post->id])}}" method="POST">

      {{-- required in updating the db  --}}
      <input type="hidden" name="_method" value="PUT">
      @csrf
         <div class='form-group'>
            
            <label for="title-input">Title</label>
            <input id="title-input" type="text" name="title" class="form-control" placeholder="Title" value="{{$post->title}}">
         </div>

         <div class='form-group'>
            <label for="body-input">Body</label>
            <textarea id="body-input" name="body" class="form-control" placeholder="Body" rows="5">{{$post->body}}</textarea>
         </div>
         <div class='form-group text-right'>
            <button type="submit" class="btn btn-dark px-5">Edit</button>
         </div>
   </form>
   @else
      <div class="container text-center">
         <h2 class="text-danger">Access Denied!</h2>
         <a href="/" class="text-muted">Back to Main Page</a>
      </div>
   @endif
@endif
@endsection