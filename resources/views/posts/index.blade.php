@extends('layouts.app')

@section('content')
<h1>All posts</h1>
 @if(count($posts) > 0)
   @foreach($posts as $post)
      <div class = "well alert alert-dark mt-2">
         <h3><a href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
         <p class="text-muted">{{$post->created_at}}</p>
         <p>{{$post->body}}</p>
      </div>
   @endforeach
   {{$posts->links()}}
   

   @else
      <p class='text-warning'>No posts found</p>
 @endif
@endsection

<!-- how to clone and start the project: -->
<!-- composer install, recreate .env from example, php artisan key:generate -->